#!/usr/bin/env python
import sys
import os
import argparse
import time
import datetime
from shutil import which
from threading import Thread
import colorama
from pathlib import Path

parser = argparse.ArgumentParser(description="A resuming timer for stand-up and jour-fixe meetings.")
parser.add_argument('-i', '--interval', type=int, help='Duration of each timer interval in seconds.', default=180)
parser.add_argument('-c', '--crew', nargs='+', metavar='M', help='Members of the present crew. If not given, timee will run indefinitely.')
parser.add_argument('-s', '--interval-sound', type=str, help='Path to the interval sound file.', default='sounds/toaster-oven-ding.wav')
parser.add_argument('-f', '--finishing-sound', type=str, help='Path to the finishing sound file.', default='sounds/fanfare.wav')
parser.add_argument('-a', '--applause-mode', dest='applause', action='store_true',
                    help='Play applause between intervals and at the end. This overrides the -s and -f flags.',)
parser.add_argument('-p', '--pause', type=int, help='Duration of pause between intervals in seconds.', default=0)
args = parser.parse_args()

colorama.init()

# Set sound paths
INTERVAL_SOUND_PATH = Path('sounds/applause.m4a') if args.applause else Path(args.interval_sound)
FINISH_SOUND_PATH = Path('sounds/applause.m4a') if args.applause else Path(args.finishing_sound)

# Set up sound playback
# TODO Paul: Try playsound instead of pydub
play_sound = True if any([which('ffplay') is not None, which('avplay') is not None]) else False
if play_sound:
    import subprocess
    import pydub.utils
    PLAYER = pydub.utils.get_player_name()
player_thread = None


def play(sound_path: Path = INTERVAL_SOUND_PATH):
    """
    Play a sound from a file
    :param sound_path: PAth to the sound file
    """
    # Pipe player output to devnull
    with open(os.devnull, 'w') as devnull:
        try:
            subprocess.call([PLAYER, "-nodisp", "-autoexit", "-hide_banner", sound_path], stdout=devnull, stderr=devnull)
        except FileNotFoundError:
            pass


# Set time slots
CREW = args.crew
RUN_INDEFINITELY = True if CREW is None else False
SLOT_TIME_SECONDS = args.interval
PAUSE_TIME_SECONDS = args.pause

# Define the length of the empty sequence for the clearing message
if RUN_INDEFINITELY:
    bro_sequence = 0
else:
    bro_sequence = max([len(bro) for bro in CREW])


# TODO Paul: Add some kind of event listening for the space key to pause the session
def run_slot(current=None, next_=None):
    if current is None:
        slot_message = ''
        next_message = ''
    else:
        possessive = current + "'s" if current[-1] != 's' else current + "'"
        slot_message = f'{colorama.Fore.GREEN}{colorama.Style.BRIGHT}{possessive}{colorama.Style.RESET_ALL} slot, '
        if next_ is None:
            next_message = " You'll be free."
        else:
            next_message = f' Next: {colorama.Fore.BLUE}{colorama.Style.BRIGHT}{next_}{colorama.Style.RESET_ALL}.'
    time_slot = datetime.timedelta(seconds=SLOT_TIME_SECONDS + 1)
    while time_slot:
        time_slot = time_slot - datetime.timedelta(seconds=1)
        if time_slot > datetime.timedelta(seconds=SLOT_TIME_SECONDS) * .1:
            time_slot_message = f'{colorama.Back.GREEN}{colorama.Fore.BLACK} {time_slot} {colorama.Style.RESET_ALL}'
        elif not time_slot:
            time_slot_message = f'{colorama.Back.RED}{colorama.Fore.BLACK} {time_slot} {colorama.Style.RESET_ALL}'
        else:
            time_slot_message = f'{colorama.Back.YELLOW}{colorama.Fore.BLACK} {time_slot} {colorama.Style.RESET_ALL}'
        sys.stdout.write(f"\r{slot_message}{time_slot_message} remaining.{next_message}")
        sys.stdout.flush()
        time.sleep(1)


def run_pause():
    time_slot = datetime.timedelta(seconds=PAUSE_TIME_SECONDS + 1)
    while time_slot:
        time_slot = time_slot - datetime.timedelta(seconds=1)
        if time_slot > datetime.timedelta(seconds=SLOT_TIME_SECONDS) * .1:
            time_slot_message = f'{colorama.Back.GREEN}{colorama.Fore.BLACK} {time_slot} {colorama.Style.RESET_ALL}'
        elif not time_slot:
            time_slot_message = f'{colorama.Back.RED}{colorama.Fore.BLACK} {time_slot} {colorama.Style.RESET_ALL}'
        else:
            time_slot_message = f'{colorama.Back.YELLOW}{colorama.Fore.BLACK} {time_slot} {colorama.Style.RESET_ALL}'
        sys.stdout.write(f"\rPause: {time_slot_message}")
        sys.stdout.flush()
        time.sleep(1)


# Run the timer
i = 0
while True:
    clear_message = ' ' * len(f"Now speaking: {' ' * bro_sequence}. 0:00:00 remaining. Next: {' ' * bro_sequence}.")
    print(f'\r{clear_message}', end='\r')
    if RUN_INDEFINITELY:
        run_slot()
    else:
        if i + 1 < len(CREW):
            run_slot(current=CREW[i], next_=CREW[i + 1])
        else:
            run_slot(current=CREW[i])
            break
    if play_sound:
        if player_thread:
            player_thread.join()
        player_thread = Thread(target=play)
        player_thread.start()
    if PAUSE_TIME_SECONDS:
        print(f'\r{clear_message}', end='\r')
        run_pause()
    i += 1

print()
play(FINISH_SOUND_PATH)
